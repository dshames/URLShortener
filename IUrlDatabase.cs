﻿using System;
using System.Collections.Generic;
using URLShortener.DataModel;

namespace URLShortener
{
    public interface IUrlDatabase
    {
        UrlDatabaseRecord GetRecord(Func<UrlDatabaseRecord, bool> predicate);
        void AddOrUpdate(string key, UrlDatabaseRecord record);
    }
}
