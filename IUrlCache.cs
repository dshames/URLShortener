﻿using System;
using System.Collections.Generic;
using URLShortener.DataModel;

namespace URLShortener
{
    public interface IUrlCache
    {
        int Count();
        bool ContainsUrlCode(string urlCode);
        bool TryAddValue(string urlCode, UrlData urlData);
        string TryGetUrl(string urlCode);
        string TryGetUrlCode(string url);
        Dictionary<string, UrlData> DumpUrlData(int count);
    }
}
