﻿using System;

namespace URLShortener.DataModel
{
    public class UrlDatabaseRecord
    {
        public string UrlCode { get; set; } // key
        public string Url { get; set; }
        public int Views { get; set; }

        public UrlDatabaseRecord(string urlCode, string url, int views)
        {
            UrlCode = urlCode;
            Url = url;
            Views = views;
        }
    }
}
