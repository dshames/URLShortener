﻿using System;
namespace Training.DataModel
{
    public class Settings
    {
        public int UrlCodeLength { get; }
        public int CacheSizeLimit { get; }
        public double CleanupPercentage { get; }

        public Settings()
        {
            // read from settings.json file and load values
        }

    }
}
