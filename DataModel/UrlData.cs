﻿using System;

namespace URLShortener.DataModel
{
    public class UrlData
    {
        public string Url { get; set; }
        public int Views { get; set; }

        public UrlData(string url, int views)
        {
            Url = url;
            Views = views;
        }
    }
}
