﻿using System;
using Training.DataModel;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Training
{
    public class UrlsCache : IUrlCache
    {
        private Dictionary<string, Tuple<UrlData, LinkedListNode<string>>> urlDataDic;
        private LinkedList<string> urlUsageOrder;
        private HashSet<int> urlHashes;

        public int Count()
        {
            return urlHashes.Count;
        }

        public bool ContainsUrlCode(string urlCode)
        {
            return urlDataDic.ContainsKey(urlCode);
        }

        public bool TryAddValue(string urlCode, UrlData urlData)
        {
            if (ContainsUrlCode(urlCode))
                return false;
            
            LinkedListNode<string> linkedListNode = new LinkedListNode<string>(urlCode);
            moveNodeToEnd(linkedListNode, true);
            urlHashes.Add(urlData.Url.GetHashCode());
            urlDataDic[urlCode] = new Tuple<UrlData, LinkedListNode<string>>(urlData, linkedListNode);
            return true;
        }

        public string TryGetUrl(string urlCode)
        {
            if (!ContainsUrlCode(urlCode))
                return null;
            
            Tuple<UrlData, LinkedListNode<string>> val = urlDataDic[urlCode];
            val.Item1.Views += 1;
            moveNodeToEnd(val.Item2);
            return val.Item1.Url;
        }

        public string TryGetUrlCode(string url)
        {
            if (!urlHashes.Contains(url.GetHashCode()))
                return null;
            
            foreach (KeyValuePair<string, Tuple<UrlData, LinkedListNode<string>>> entry in urlDataDic)
            {
                if (entry.Value.Item1.Url == url)
                {
                    moveNodeToEnd(entry.Value.Item2);
                    return entry.Key;
                }
            }

            return null;
        }

        private void moveNodeToEnd(LinkedListNode<string> node, bool newNode = false)
        {
            // ignoring edge cases where node is first or last..
            if(!newNode)
            {
                node.Previous.Next = node.Next;
                node.Next.Previous = node.Previous;
            }

            node.Previous = urlUsageOrder.Last;
            node.Next = null;
            usageOrder.Last.Next = node;
        }

        public Dictionary<string, UrlData> DumpUrlData(int count)
        {
            Dictionary<string, UrlData> dump = new Dictionary<string, UrlData>();

            for (int i = 0; i < count; i++)
            {
                string key = urlUsageOrder.First.Value;
                UrlData urlData = urlDataDic[key].Item1;
                dump[key] = urlData;

                urlUsageOrder.RemoveFirst();
                urlHashes.Remove(urlData.Url.GetHashCode());
                urlDataDic.Remove(key);
            }
            return dump;
        }

    }
}
