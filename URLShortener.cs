﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using URLShortener.DataModel;

namespace URLShortener
{
    public class URLShortener
    {
        private const string charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        private int urlCodeLength;
        private int cacheSizeLimit;
        private double cleanupPercentage;

        private IUrlCache urlCache;
        private IUrlDatabase database;

        public URLShortener(IUrlCache cache, IUrlDatabase db, Settings settings)
        {
            urlCache = cache;
            database = db;

            urlCodeLength = settings.UrlCodeLength;
            cacheSizeLimit = settings.CacheSizeLimit;
            cleanupPercentage = settings.CleanupPercentage;
        }

        public string GetUrlCode(string url)
        {
            string urlCode = urlCache.TryGetUrlCode(url);
            if (urlCode != null)
                return urlCode;

            UrlDatabaseRecord record = database.GetRecord(r => r.Url == url);
            if (record == null)
            {
                urlCode = generateUrlCode();
                urlCache.TryAddValue(urlCode, new UrlData(url, 0));
            }
            else 
            {
                urlCode = record.UrlCode;
                urlCache.TryAddValue(urlCode, new UrlData(record.Url, record.Views));
            }

            dumpCacheIfNeeded();
            return urlCode;
        }
        public string GetUrl(string urlCode)
        {
            string url = urlCache.TryGetUrl(urlCode);
            if (url != null)
                return url;

            UrlDatabaseRecord record = database.GetRecord(r => r.UrlCode == urlCode);
            if (record != null)
                return null; // http response with status code 404
            
            urlCache.TryAddValue(record.UrlCode, new UrlData(record.Url, record.Views));
            dumpCacheIfNeeded();

            // http response with status code 302, location: record.Url
            return record.Url;
        }

        private string generateUrlCode()
        {
            Random random = new Random();
            StringBuilder stringBuilder = new StringBuilder();

            do
            {
                for (int i = 0; i < urlCodeLength; i++)
                {
                    char c = charset[random.Next(charset.Length)];
                    stringBuilder.Append(c);
                }
            } while (isUrlCodeInUse(stringBuilder.ToString()));

            return stringBuilder.ToString();
        }

        private bool isUrlCodeInUse(string urlCode)
        {
            if (urlCache.ContainsUrlCode(urlCode))
                return true;

            if (database.GetRecord(r => r.UrlCode == urlCode) != null)
                return true;

            return false;
        }

        private void dumpCacheIfNeeded()
        {
            if (urlCache.Count() >= cacheSizeLimit)
            {
                int count = Convert.ToInt32(urlCache.Count() * cleanupPercentage);
                addDumpedUrlsToDb(urlCache.DumpUrlData(count));

            }
        }

        private void addDumpedUrlsToDb(Dictionary<string, UrlData> dumpedUrls)
        {
            foreach (string key in dumpedUrls.Keys)
            {
                database.AddOrUpdate(key, new UrlDatabaseRecord(key, dumpedUrls[key].Url, dumpedUrls[key].Views));
            }
        }
    }
}
